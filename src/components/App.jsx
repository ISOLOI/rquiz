import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import QuestionList from './quiz/QuestionList.jsx';
import Scorebox from './quiz/Scorebox.jsx';
import Results from './quiz/Results.jsx';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      questions: [
        {
          id: 1,
          text: 'Who is this quiz about?',
          choices: [
            {
              id: 'a',
              text: 'Michael'
            },
            {
              id: 'b',
              text: 'Nick Kaufmann'
            },
            {
              id: 'c',
              text: 'Izzy'
            },
          ],
          correct: 'b'
        },
        {
          id: 2,
          text: 'Why should you hire him?',
          choices: [
            {
              id: 'a',
              text: 'Good Looks'
            },
            {
              id: 'b',
              text: 'Pretty Eyes'
            },
            {
              id: 'c',
              text: 'Very Knowledgeable in Web Development'
            },
          ],
          correct: 'c'
        },
        {
          id: 3,
          text: 'When should you hire him?',
          choices: [
            {
              id: 'a',
              text: 'ASAP'
            },
            {
              id: 'b',
              text: 'Wait a couple weeks'
            },
            {
              id: 'c',
              text: 'Hire the Wrong Person'
            },
          ],
          correct: 'a'
        },
        {
          id: 4,
          text: 'Do you want to succeed?',
          choices: [
            {
              id: 'a',
              text: 'Is an Elephant heavy?'
            },
            {
              id: 'b',
              text: 'Always'
            },
            {
              id: 'c',
              text: 'All The Above'
            },
          ],
          correct: 'c'
        },
      ],
      score: 0,
      current: 1
    }
  }

setCurrent(current){
  this.setState({current});
}

setScore(score){
  this.setState({score});
}
render(){
  if(this.state.current > this.state.questions.length){
    var scorebox = '';
    var results = <Results {...this.state} />
  } else {
    var scorebox = <Scorebox {...this.state} />
    var results = '';
  }

      return(
        <div>
          {scorebox}

        <QuestionList {...this.state} setCurrent={this.setCurrent.bind(this)} setScore={this.setScore.bind(this)} />
        {results}
      </div>
      )
    }
  }

export default App
